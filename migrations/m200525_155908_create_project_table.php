<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%booking}}`.
 */
class m200525_155908_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'userid' => $this->integer()->notNull(),
            'title' => $this->string(),
            'price' => $this->integer(),
            'datanach' => $this->date(),
            'datakonec' => $this->date(),
        ]);
   
        $this->addForeignKey(
            'userid',  
            'project', 
            'userid', 
            'user', 
            'id', 
            'CASCADE'
        );
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('project');
      
    }
}
