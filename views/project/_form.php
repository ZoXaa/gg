<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
        echo $form->field($model, 'userid')->dropdownList(
        User::find()->select(['name', 'id'])->indexBy('id')->column()
    ); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'datanach')->textInput([ 'value' => date('Y-m-d')]) ?>

    <?= $form->field($model, 'datakonec')->textInput([ 'value' => date('Y-m-d')]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
